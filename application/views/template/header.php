<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><?php echo $this->config->item("title"); ?></title>
  <link rel="icon" href="<?php echo base_url("images/favicon.ico"); ?>">
  <link href="<?php echo base_url("assets/vendor/node_modules/bootstrap/dist/css/bootstrap.min.css"); ?>" rel="stylesheet"/>
  <link href="<?php echo base_url("assets/vendor/node_modules/angular-material/angular-material.min.css"); ?>" rel="stylesheet">
  <link href="<?php echo base_url("assets/css/styles.css"); ?>" rel="stylesheet"/>
</head>
<body>